package com.zuitt;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");

        try {
            int num = in.nextInt();
            int answer = 1;
            int variable = 1;

            while (variable < num + 1) {
                answer = answer * variable;
                variable++;
            }

            System.out.println("The factorial of " + num + " is " + answer);
        } catch (Exception e) {
            System.out.println("Only numbers are accepted for this system");
        }

    }
}
